FROM ruby:latest
WORKDIR /usr/src/app
COPY Gemfile Gemfile.lock ./
RUN gem install bundler && bundle install
CMD ["bundle", "exec", "jekyll", "build"]
